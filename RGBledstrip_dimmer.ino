
#include <EEPROM.h>
#include <NewRemoteReceiver.h>

namespace trick17 {};
#define DEBUG

#define BLINK 13

// INTERRUPTS
// Note the difference between the Arduino boards
//Arduino Leonardo: PIN2 = INT1
//Arduino Pro Mini: PIN2 = INT0
#define INT_LEARN 0 
//Arduino Leonardo: PIN3 = INT0
//Arduino Pro Mini: PIN3 = INT1
#define INT_RECEIVER 1 
#define REDPIN 5
#define GREENPIN 6
#define BLUEPIN 9

// Unit on remote TODO learn which button it is!
#define RED_UNIT 8
#define GREEN_UNIT 9
#define BLUE_UNIT 10



int color[] = {0, 0, 0};
unsigned long dimmerMillis[] = {0, 0, 0};

unsigned long address = 0;
unsigned long addresses[] = {0, 0, 0, 0};

unsigned long blinkstart = 0; //the last time the LED was updated
int blinkinterval = 250; // How long the LED stays on/off
int ledState = LOW;
int ledMode = 0; //0 = blink : 1 = flash

#ifdef DEBUG
int serialinterval = 500;
#endif

// LEARN
int learnperiod = 6000;
unsigned long learnstart = 0;

void setup() {
	Serial.begin(115200);

	//Restore values
	readaddressesFromEEPROM();

	// When learnbutton is pressed call learn interupt routine
	pinMode(2, INPUT); // LEARNBUTTON
	digitalWrite(2, LOW);
	attachInterrupt(INT_LEARN, startLearn, RISING);

	// Clear address memory
	//EEPROM_writelong(0, 0);
	//EEPROM_writelong(4, 0);
	//EEPROM_writelong(8, 0);
	//EEPROM_writelong(12, 0);

	// Initialize receiver on interrupt 0 (= digital pin 3), calls the callback "showCode"
	// after 2 identical codes have been received in a row. (thus, keep the button pressed
	// for a moment)
	//

	// See the interrupt-parameter of attachInterrupt for possible values (and pins) to connect the receiver.
	pinMode(3, INPUT); // RECEIVER
	NewRemoteReceiver::init(INT_RECEIVER, 2, showCode);

	pinMode(BLINK, OUTPUT);

	pinMode(REDPIN, OUTPUT);
	pinMode(GREENPIN, OUTPUT);
	pinMode(BLUEPIN, OUTPUT);
	interrupts();


}

void loop() {

	blinkLED();
	dimmer();



	// If learnperiod is over stop it
	if (learnstart != 0 && millis() - learnstart > learnperiod) stopLearningPeriod();
	delay(10);
}

// Callback function is called only when a valid code is received.
void showCode(NewRemoteCode receivedCode) {

	// If no valid address is received and not in learing period... ignore received code
	if (learnstart == 0 && getaddressIndex(receivedCode.address) == -1) return;

#ifdef DEBUG
	// Print the received code.
	Serial.print("Addr ");
	Serial.print(receivedCode.address);

	if (receivedCode.groupBit) {
		Serial.print(" group");
	}
	else {
		Serial.print(" unit ");
		Serial.print(receivedCode.unit);
	}
#endif


	//Blink led if received something
	blinkstart = millis();

	// color index RGB
	int index = 0;
	switch (receivedCode.unit) {
	case RED_UNIT :
		index = 0;
		break;
	case GREEN_UNIT :
		index = 1;
		break;
	case BLUE_UNIT :
		index = 2;
		break;
	}

	switch (receivedCode.switchType){

	case NewRemoteCode::off :
		// OFF Button Presses

		// When in learnperiod REMOVE address
		if (learnstart > 0) {
			removeAddress(receivedCode.address);
			stopLearningPeriod();
		}
		// LED strip of
		else {
#ifdef DEBUG
			Serial.print(",    off");
#endif
			color[index] = 0;
			setColor(index);
		}
		break;

	case NewRemoteCode::on:
		// When in learnperiod SAVE address
		if (learnstart > 0) {
			saveAddress(receivedCode.address);
			stopLearningPeriod();
		}
		else if (dimmerMillis[index] > 0) {
			// Stop dimming
#ifdef DEBUG
			Serial.print(", dimOff");
#endif
			dimmerMillis[index] = 0;
		}
		else {
			// Start dimming
#ifdef DEBUG
			Serial.print(", dimOn");
#endif
			dimmerMillis[index] = millis();
		}
		break;
		break;
		// DIM code received
	case NewRemoteCode::dim:
		if (receivedCode.dimLevelPresent) {
			setColor(index, receivedCode.dimLevel);
#ifdef DEBUG
			Serial.print(", dimLevel: ");
			Serial.print(receivedCode.dimLevel);
#endif
		}
		break;
	}

#ifdef DEBUG
	showColors();
#endif


}

void blinkLED() {
	// BLINK

	unsigned long currentMillis = millis();
	if(ledMode == 0){
		if (currentMillis - blinkstart > blinkinterval)
			digitalWrite(BLINK, LOW);
		else
			digitalWrite(BLINK, HIGH);
	}
	// FLASH
	else {
		//if the LED is already off
		if (ledState == LOW){
			//and enough time has passed
			if (currentMillis - blinkstart > blinkinterval){
				digitalWrite(BLINK, HIGH);
				//store its current state
				ledState = HIGH;
				//update the time of this new event
				blinkstart = currentMillis;
			}
		} else {//if the LED is already on
			if (currentMillis - blinkstart > blinkinterval){
				digitalWrite(BLINK, LOW);
				ledState = LOW;
				blinkstart = currentMillis;
			}
		}


	}
}

void dimmer() {
	for (int i = 0; i < 3; i++) {
		if (dimmerMillis[i] > 0) {
			unsigned long currentMillis = millis();
			if ((currentMillis - dimmerMillis[i]) > 20) {
				color[i] += 1;
				dimmerMillis[i]  = currentMillis;
			}
			setColor(i);
		}
	}

}

void setColor(int i) {
	int PIN;
	if (i == 0) PIN = REDPIN;
	if (i == 1) PIN = GREENPIN;
	if (i == 2) PIN = BLUEPIN;

	if (color[i] == 510) {
		color[i] = 0;
	}
	else if (color[i] > 255) {
		analogWrite(PIN, (255 - (color[i] - 255)));
	}
	else {
		analogWrite(PIN, color[i]);
	}

}

// value between 0 and 15
void setColor(int i, int value) {
	color[i] = (value *17);
	dimmerMillis[i] = 0;
	setColor(i);
}

void startLearn() {


	if (learnstart == 0) {
		detachInterrupt(INT_LEARN);
		// Flash LED when in learning period
		ledMode = 1;
		learnstart = millis();
#ifdef DEBUG
		Serial.println("START LEARNING PERIOD");
		showAdresses();
#endif
	}
}



void stopLearningPeriod() {
	ledMode = 0;
	blinkstart = 0;
	blinkLED();
	learnstart = 0;
	attachInterrupt(INT_LEARN, startLearn, RISING);
#ifdef DEBUG
	Serial.println("STOP LEARNING PERIOD");
	showAdresses();
#endif
}


//Read 4 addresses from EEPROM
void readaddressesFromEEPROM(){

	for(int i = 0; i< 4; i++){
		// A long is four bytes
		addresses[i] = EEPROM_readlong(i*4);

	}
	showAdresses();
}

void showAdresses(){

#ifdef DEBUG
	Serial.print("Addrs:\t\t\t");

	for(int i = 0; i< 4; i++){

		Serial.print(addresses[i]);
		Serial.print("\t\t");

	}

	Serial.println();
#endif
}

void saveAddress(unsigned long adr) {
	int i = getFirstEmptyAddressIndex();
	if (i != -1)
		EEPROM_writelong(i*4, adr);

	showAdresses();

}

void removeAddress(unsigned long adr) {
	int i = getaddressIndex(adr);
	if (i != -1)
		EEPROM_writelong(i*4, 0);

	showAdresses();

}

// Return index of address or  -1 when not exists 
int getaddressIndex(unsigned long adr) {
	for(int i = 0; i< 4; i++){
		if (addresses[i] == adr ) return i;
	}
	return -1;
}
int getFirstEmptyAddressIndex() {
	int empty = -1;
	for(int i = 0; i< 4; i++){
		if (addresses[i] == 0 ) return i;
	}
	return empty;
}


#ifdef DEBUG
void showColors() {

	Serial.print("\t\t\tR ") ;
	Serial.print(color[0]);
	Serial.print(" G ") ;
	Serial.print(color[1]);
	Serial.print(" B ") ;
	Serial.println(color[2]);

}
#endif



// read double word from EEPROM, give starting address
unsigned long EEPROM_readlong(int address) {
	//use word read function for reading upper part
	unsigned long dword = EEPROM_readint(address);
	//shift read word up
	dword = dword << 16;
	// read lower word from EEPROM and OR it into double word
	dword = dword | EEPROM_readint(address + 2);
	return dword;
}

//write word to EEPROM
void EEPROM_writeint(int address, int value) {
	EEPROM.write(address, highByte(value));
	EEPROM.write(address + 1 , lowByte(value));
}

//write long integer into EEPROM
void EEPROM_writelong(int address, unsigned long value) {
	//truncate upper part and write lower part into EEPROM
	EEPROM_writeint(address + 2, word(value));
	//shift upper part down
	value = value >> 16;
	//truncate and write
	EEPROM_writeint(address, word(value));
}

unsigned int EEPROM_readint(int address) {
	unsigned int word = word(EEPROM.read(address), EEPROM.read(address + 1));
	return word;
}


