//This is a automatic generated file
//Please do not modify this file
//If you touch this file your change will be overwritten during the next build
//This file has been generated on 2015-01-31 12:46:13

#include "Arduino.h"
#include <EEPROM.h>
#include <NewRemoteReceiver.h>
void setup() ;
void loop() ;
void showCode(NewRemoteCode receivedCode) ;
void blinkLED() ;
void dimmer() ;
void setColor(int i) ;
void setColor(int i, int value) ;
void startLearn() ;
void stopLearningPeriod() ;
void readaddressesFromEEPROM();
void showAdresses();
void saveAddress(unsigned long adr) ;
void removeAddress(unsigned long adr) ;
int getaddressIndex(unsigned long adr) ;
int getFirstEmptyAddressIndex() ;
void showColors() ;
unsigned long EEPROM_readlong(int address) ;
void EEPROM_writeint(int address, int value) ;
void EEPROM_writelong(int address, unsigned long value) ;
unsigned int EEPROM_readint(int address) ;


#include "RGBledstrip_dimmer.ino"
